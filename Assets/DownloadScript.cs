﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Runtime.InteropServices;

public class DownloadScript : MonoBehaviour {

    public InputField urlTxt, nameAB;
    public Slider slider;
    
    public void LoadAssetBundle()
    {
        Debug.Log(urlTxt.text);
        Debug.Log(nameAB.text);
        StartCoroutine(DownLoadAssetBundle());
    }

    IEnumerator DownLoadAssetBundle()
    {
        UnityWebRequest unityWebRequest = UnityWebRequestAssetBundle.GetAssetBundle(urlTxt.text, 0);
        yield return unityWebRequest.SendWebRequest();

        while (!unityWebRequest.isDone)
        {
            slider.value = unityWebRequest.downloadProgress;
            yield return null;
        }

        if(unityWebRequest.isHttpError || unityWebRequest.isHttpError)
        {
            Debug.Log(unityWebRequest.error);
        }
        else
        {
            slider.gameObject.SetActive(false);
        }

        AssetBundle myLoadedAssetBundle = DownloadHandlerAssetBundle.GetContent(unityWebRequest);
        GameObject obj = Instantiate(myLoadedAssetBundle.LoadAsset(nameAB.text)) as GameObject;
    }

    [DllImport("__Internal")]
    private static extern void Hello();

    [DllImport("__Internal")]
    private static extern void HelloString(string str);

    public void CallAlert()
    {
        Hello();
        HelloString("This is a string.");

    }
}
